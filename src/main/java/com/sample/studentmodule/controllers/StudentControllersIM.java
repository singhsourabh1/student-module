package com.sample.studentmodule.controllers;

import java.util.List;

import com.sample.studentmodule.models.Student;
import com.sample.studentmodule.services.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/student")
public class StudentControllersIM {
    @Autowired
    private StudentService service;

    @GetMapping("/list")
    public List<Student> list() {
        return service.listStudentsInMemory();
    }

    @PostMapping("/create")
    public Student create(@RequestBody Student student) {
        return service.createStudentIMemory(student);
    }

    @GetMapping("/search")
    public List<Student> search(@RequestParam String name) {
        return service.searchStudentIMemoy(name);
    }
}
