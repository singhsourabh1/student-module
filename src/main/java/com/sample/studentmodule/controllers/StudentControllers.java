package com.sample.studentmodule.controllers;

import java.util.List;

import com.sample.studentmodule.models.Student;
import com.sample.studentmodule.services.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v2/student")
public class StudentControllers {
    @Autowired
    private StudentService service;

    @GetMapping("/list")
    public List<Student> list() {
        return service.listStudents();
    }

    @PostMapping("/create")
    public Student create(@RequestBody Student student) {
        return service.createStudent(student);
    }

    @GetMapping("/search")
    public List<Student> search(@RequestParam String name) {
        return service.searchStudent(name);
    }
}
