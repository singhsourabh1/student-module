package com.sample.studentmodule.models;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Student {
    @Id
    @GeneratedValue
    private UUID id;

    @Column(length = 50)
    private String name;

    private int division;

    private int rollNo;
}
