package com.sample.studentmodule.services;

import java.util.List;

import com.sample.studentmodule.models.Student;
import com.sample.studentmodule.repositories.StudentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentInMemory;

    @Autowired
    private StudentRepository studentJPAImpl;

    public List<Student> listStudentsInMemory() {
        return studentInMemory.list();
    }

    public Student createStudentIMemory(Student student) {
        return studentInMemory.create(student);
    }

    public List<Student> searchStudentIMemoy(String name) {
        return studentInMemory.searchByName(name);
    }

    public List<Student> listStudents() {
        return studentJPAImpl.list();
    }

    public Student createStudent(Student student) {
        return studentJPAImpl.create(student);
    }

    public List<Student> searchStudent(String name) {
        return studentJPAImpl.searchByName(name);
    }
}