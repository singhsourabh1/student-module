package com.sample.studentmodule.repositories.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.sample.studentmodule.models.Student;
import com.sample.studentmodule.repositories.StudentRepository;

import org.springframework.stereotype.Repository;

@Repository
public class StudentInMemory implements StudentRepository {
    private List<Student> students;

    public StudentInMemory() {
        students = new ArrayList<>();
    }

    public List<Student> list() {
        return students;
    }

    public Student create(Student student) {
        student.setId(UUID.randomUUID());
        students.add(student);
        return student;
    }

    public List<Student> searchByName(String q) {
        return students.stream().filter(el -> el.getName().toLowerCase().contains(q.toLowerCase()))
                .collect(Collectors.toList());
    }
}
