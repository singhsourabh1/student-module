package com.sample.studentmodule.repositories.impl;

import java.util.List;
import java.util.UUID;

import com.sample.studentmodule.models.Student;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentJPA extends JpaRepository<Student, UUID> {
    List<Student> findByNameContaining(String name);
}
