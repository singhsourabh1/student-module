package com.sample.studentmodule.repositories.impl;

import java.util.List;

import com.sample.studentmodule.models.Student;
import com.sample.studentmodule.repositories.StudentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

@Repository
public class StudentJPAImpl implements StudentRepository {
    @Lazy
    @Autowired
    private StudentJPA studentJPA;

    @Override
    public Student create(Student student) {
        return studentJPA.saveAndFlush(student);
    }

    @Override
    public List<Student> list() {
        return studentJPA.findAll();
    }

    @Override
    public List<Student> searchByName(String q) {
        return studentJPA.findByNameContaining(q);
    }
}
