package com.sample.studentmodule.repositories;

import java.util.List;

import com.sample.studentmodule.models.Student;

public interface StudentRepository {
    List<Student> list();

    Student create(Student student);

    List<Student> searchByName(String q);
}