# Student Module

----

## Description

In this project includes api to create, list and search student.

Datastorage is done through:

1.  In-memory
2.  Persistence (MySQL) 

----

## Postman Collection

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/e6a9d693e60a2372010d)

__Note:__ All v1 apis uses in-memory implementation and v2 uses mysql as datastore.